---
layout: page
title: About
permalink: /about/
---

<img src="/assets/about.jpg"/>


Hi, I'm Giridhur Sriraman. My pronouns are he/his/him and you can call me Giri (pronounced gee-ree)

I have a large social media footprint, you can find me on:

[Linkedin](https://www.linkedin.com/in/giridhur/)
[Twitter](https://twitter.com/irishdrug1511)
[Instagram](https://www.instagram.com/irishdrug1511/)

You can also reach me on email [here](mailto:email@giridhurs.com&subject="Contact")

This page is a work in progress, like my life. Come back soon!

Best,
GS