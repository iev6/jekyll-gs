---
layout: page
title: Home
id: home
permalink: /
---

# Welcome! ☘️

I'm Giri and welcome to my online home. [[hello]]! 

What would you like to read?

Here are some of my favorite posts
1. [[antichess]] 
2. [[seeking]] 

Here's a graph of all the posts so far, click on the ones you'd like to read!

{% include notes_graph.html %}

<style>
  .wrapper {
    max-width: 46em;
  }
</style>
