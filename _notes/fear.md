---
layout: note
title: fear
categories: advice
---

“I must not fear. Fear is the mind-killer. Fear is the little-death that brings total obliteration. I will face my fear. I will permit it to pass over me and through me. And when it has gone past I will turn the inner eye to see its path. Where the fear has gone there will be nothing. Only I will remain.”

<img src="/assets/dune-fear.png" style="width:250px">


Fear is a debilitating emotion. You conjure a version of reality that strips you of all power. 