---
layout: note
title: fitness
date updated: '2023-06-18'
---


Here're some fitness routines that I've been trying out. Pick what works for you!

- Body
	- Strength
	- Flexibility
	- Mobility
- Mind
	- Resilience
	- Equanimity