---
layout: note
title: Seeking the right things
categories: advice
---

An underrated struggle in life is validating our (sometimes, life altering) choices. When all you have is pure uncertainty in front of you and there are so many paths, you don’t know what to pick. Even worse, you don’t know which one of them spells doom, like the game of snake and ladders, except you’re not throwing a die here, or are you?

I know, I know and I too have read the countless books on decision making, thinking rationally and what not, but sometimes it’s difficult to do it especially when you think your whole life is at risk. I can sum up the entire fear in a single phrase — “What If?” We conjure all the good & bad outcomes of our choices, and our lizard brain emphasises the latter even more. While it’s good to correct this thermometer by listing things down and taking count of which outcomes are legit & which are not, it’s difficult to go past the doubt that tells you this decision could be sub-optimal. After all there’s always a “choice” of not making the decision, and let things take their course on autopilot. A few of the times the world crumbles and mostly nothing happens except you’re left with feeling regret. 

It might seem funny but, trying to see the tree of possibilities from above could help, and understand that nothing is static, not even you. You’re the medium, being moulded by the decisions you make, you either become Michelangelo’s David or you take the shape of a humble pot to hold water for a thirsty crow to drink from. Suppose you have a ticket to a new country and you are unsure of what’s ahead. You have a new house, new people, new food, (maybe) new languages to experience. On the other hand, you’re well settled in your niche, comfortable enough to pursue new hobbies, build yourself and surround yourself with people you like. There is sufficient depth in each branch, you just have to lean in to enjoy. In weaving your life’s tapestry thread by thread, a different colour or a different pattern will change the beauty of your creation.  

There are some caveats, as always. We live a life with constraints, some dealt to us, some emerge from our own beliefs. While we can’t do much about the ones we’re dealt with, accepting them goes a long way in empowering ourselves. We may build our identities around them, but remember, you can discard all your beliefs and build everything from scratch. Belief in how much money you want to earn, in who you want to marry and in what you want to do for a living, all of these are (usually) just beliefs, and can be negotiated (with ourselves). Sometimes, a positive belief about an experience is enough to manifest the reality you want.


Good luck!

giridhur s.

--

Go back - [[advice]]