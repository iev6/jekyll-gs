---
layout: note
title: Philosophy of Life
categories: advice
---

After close to a quarter century of being, I don't yet fully understand how my mind and body work. And I hope to get to know them better day-by-day, my mind and body are the two assets in this universe I have in my complete control after all. 

As usual, I seek to borrow knowledge from people who have trodden this path before. This path has a name - philosophy and has many varieties to pick form. I choose stoicism and I will use my own flavor which will continue to evolve as I grow older (and wiser, hopefully), for change is the only permanent thing in this world.

I perceive stoicism as a philosophy that rests on three simple tenets :

1. The past has happened, live in the present and focus on the future.
2. Seek to understand, in every situation, what is in your control and what isn't.
3. In any event in your life, you control the perception and understanding of information.

1 -> Tells me to understand that time is fleeting, so I must enjoy and make the best of the present and live life without regrets, and at the same time love my future self and act accordingly. I will honor my promises to my future self, and not procrastinate in any action. 

2 -> Tells me to look at any situation as a 3rd person RPG. I only have my mind, my body and my knowledge in my control; I can only influence everything else, to some degree with my mind, body and knowledge.

3 -> Tells me to apply a growth & positive-sum mindset to any situation in life. I get to learn from every minute, every interaction and every happening, and that will make me a better person, and bring happiness to my future self. 

As easy as it is to write about this, following the tenets lies on how deeply I love my future self. The more I do, better I internalize this and the happier I become. I believe Happiness is the ultimate manifestation of entropy, there is no limit.

---
