---
layout: note
title: Valedictorian Speech
date updated: '2022-02-01T14:47:48+05:30'
categories: nostalgia

---

I was asked to deliver a 2min speech to my graduating class in IIT Madras in 2018. The date was a fortunate coincidence with the anniversary of the Moon Landing & I took some personal liberty in sampling a portion of Rick Astley's "Never Gonna Give You Up"

[Youtube Link](https://youtu.be/W4mDNbw087s)  _not a rick roll_

This is the transcript of the speech I delivered.

--- 
Good afternoon,

I'm extremely honoured and deeply thankful to the institute for conferring this award. I owe this award to my professors, friends  and family who have guided me through college and it'd serve as a memento of the exhilarating 5 years in insti.

I can't help but wonder how quickly time flies, it's been almost half a decade when I saw my seniors returning their gown, on the day I had come to register myself in the institute. This very building contained many of the people here, and we were formally out of school into the wild unknown.

A senior alumnus once told us, we students are diamonds in the rough, and insti is a process that excels at crafting each of us into unique and valuable beings who push the boundaries of humanity. Only now do I realize how that has been true, every course, every quiz, exam, internship, project, every single minute has contributed in making every single one of us into  all-round-all-weather performers.

One more thing. While this day might be about us students, transitioning into a new phase of life, it wouldn't be complete without acknowledging the constant unwavering support of those who kept pushing us to strive for excellence, and took care of all that we needed. Today is their day, it is for our parents, siblings and relatives, and this day is an award to their presence. A round of applause for the people who've always been there for us. Thank you. 

I'd like to end this by dedicating a song to my insti - 

```

never gonna give you up
never gonna run you down
and I'm never gonna say goodbye

```


---

Comments (& appreciation if any) welcome [here](mailto:email@giridhurs.com&subject="Valedictorian Speech") 

