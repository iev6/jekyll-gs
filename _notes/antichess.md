---
layout: note
title: "Antichess : The art of learning to let go"
date: 2021-09-28T18:59:32+05:30
categories: hobbies
date updated: 2021-10-11T03:34:34+05:30
tags: []
---

Hello again,

What would you give to play a game where your objective is to lose? Let go of all your resources, and surrender to the opponent without anyone on your side?

Antichess, (also called Give-away chess) is a twisted variant of regular Chess where you win by losing all your pieces first. I discovered this on lichess.org after being discontent with normal chess., and I'm a fan forever. There are some more rules: pawns can be promoted to anything (including a king), no moves imply a win, only way for a legal draw (opposite bishops) and of course, white goes first (spoiler alert : this ruins fairness of the game).

I love this game for these three reasons :

1. Games are very quick! Most games end within 5mins total., and perfect for breaks between meetings at work.
2. It is significantly _easier_ to master (compared to chess), the total state-space (# of distinct games) is relatively small, while complex enough to keep the game fun.
3. Blunders are very common & completely swing the game around. It makes it worth while to stay & hope for your opponent to make a mistake.

This is a glimpse of what a match is like, use arrow keys/scroll through the moves list :

<iframe src="https://lichess.org/embed/fOziYQ7n?theme=brown&bg=dark"       
onload='javascript:(function(o){o.style.height=o.contentWindow.document.body.scrollHeight+"px";}(this));' style="height:200px;width:100%;border:none;overflow:hidden;"></iframe> 

The game much like its rules, is very quirky :

Like most games with defined board states, Antichess is "solved", meaning, in theory all trails through the forest of states are known (there is no information asymmetry between the players).

Turns out, it is always possible for white (which moves first) to enforce a win by playing e3 [^1], the game tree is around 2GB or so in size.My kudos to Cătălin Frâncu for their impressive [opening guide](https://catalin.francu.com/nilatac/book.php) and to the authors of these posts in lichess forums [#1](https://lichess.org/study/FRtr4n6K) [#2](https://lichess.org/forum/team-the-antichess-club/antichess-tip-sheet-the-one-i-mail-to-new-ish-players-that-show-interest-in-the-game)

An interesting consequence of the above is that there is an artificial limit to what anyone's maximum [rating](https://en.wikipedia.org/wiki/Elo_rating_system) can be; if coin flip decides you play black you lose, else you win. The rating difference between the top two players converges to the maximum rating increment [^t]

Often in life, we don't know what actions can take us to a win in situations, but steps leading to failure are apparent. Antichess makes it deceptively simple to see this, flip the board & observe from your opponent's perspective.

PS : Hit me up on [lichess](https://lichess.org/@/giridhurs), for a game anytime!

godspeed,

giridhur.s


[^t]:  #todo make a python script to arrive at the value through simulation
[^1]: [Link](http://magma.maths.usyd.edu.au/~watkins/LOSING_CHESS/browse.html) to an opening explorer (Desktop only).
