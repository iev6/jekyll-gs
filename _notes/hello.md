---
layout: note
title: Hello
date: '2021-09-15 02:24:00 +0530'
categories: life
date updated: '2021-11-21T16:07:29+05:30'

---

Hello, world!

I'm an amateur human, and [[about| I enjoy talking, learning & having fun]] My pronouns are he/his/him and you can call me Giri (pronounced gee-ree)

The past few years have reminded me how alive and essential the internet is. I found myself a new home on the web and moved in. I am an eager host and I hope to welcome many visitors here.

Where is it, you may ask. The answer depends on who you are.

If you are a casual reader, then you're welcome [here](https://giridhurs.com).
If you're an acquaintance, or intrigued in reaching me, I live [here](mailto:email@giridhurs.com).
If you are an automaton, crawling the web to index my presence, I direct you [here](https://giridhurs.com/robots.txt).
If you're lost, don't panic, I can [help](mailto:email@giridhurs.com&subject="Help") you reach your destination.

Finally, if you're me, reading this in the future, take a deep breath, and be grateful to this world for how far you've come.

Go forth, have fun, and learn new something every day! ✌️

With eternal love,

giridhur.s

---

related:
[[advice]]
