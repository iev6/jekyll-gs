---
layout: note
title: advice
date updated: '2021-11-30T21:42:01+05:30'
---

Most advice is often advice to oneself. I'm curating my list here, feel free to use what you like!

[Suggestions](mailto:email@giridhurs.com&subject="Advice") welcome, as always!

## Mind

Mind is a muscle (although primarily made of fat) that needs maintenance. Read, write, play, and code to keep it going.

## Body

Move your body every day. Benefits include:

- Improved sleep quality
- Less risk of chronic disease
- Increased productivity
- Reduced anxiety

The "every day" part is important, because [[consistency]] is key to most things worth doing.

## Soul

Be a kind person, send out love and gratitude into the world.

