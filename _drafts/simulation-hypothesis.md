---
layout: post
title: Simulation Hypothesis
date: 2021-09-24 23:56:00 +0530
categories: thoughts
date updated: '2021-09-25T00:06:50+05:30'
---

> Do you believe in fate, Neo?


Belief in free will is very reassuring, and gives us a sense of control. After all,

> I am the master of my fate, 
> I am the captain of my soul.
>  - William Ernest Henley, Invictus