---
layout: post
title: 2024-goals
date updated: 2023-11-26
---
Walking into 2024, i want to have audacious goals, beginning with
1. Going on an international solo trip : to find out if i can make a living in a desired country outside india
2. Getting stronger : to be able to lift heavier, lose unneeded weight, and run faster
	1. Weights - Bench Press : 1x BW, Deadlift : 2x BW, Squat : 2x BW
	2. Skills - 10k in 1 hr, one arm pull up & flexibility
3. Creating content : to write more for myself & others, maybe speak/shoot videos
4. Launch a revenue generating ML product


I'll report my progress, achievements, misses here. Let's see how it goes.


Cheers to 2024! 